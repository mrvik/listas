#!/usr/bin/env node
/* eslint-env node */
/* eslint-disable no-sync */
const fs=require("fs-extra");
const path=require("path");

const dest=process.argv[2]||"dist";

var modules={
    "loadlify": "node_modules/loadlify/loadlify.min.js"
};
const reg=new RegExp(/(\/\*Begin Loadlify defs\*\/)([\s\S]*)(\/\*End Loadlify defs\*\/)/);

const loadlify={
    defs:{}
};
const code=fs.readFileSync("index.html", "UTF-8")
    .match(reg);
if(code===null||!code[2]){
    throw new Error("No definitions found on index.html");
}
const fn=new Function("loadlify", code[2]);
fn(loadlify);

modules={
    ...modules,
    ...loadlify.defs
};

console.info("Packing the following items:");
console.table(modules);

(async ()=>{
    let newpath;
    for(let i in modules){
        let p=modules[i];
        newpath=path.join(dest,p);
        console.info(`Copying ${p} to ${newpath}`);
        await fs.ensureFile(newpath);
        await fs.copyFile(p, newpath);
    }
})();
