/* eslint-env serviceworker */
"use-strict";

/**
 * Warning: This Service Worker is untested, so may not work, please, don't use it directly on production
 */
var getResponse;
let props={
    name: "listas",
    version: 0.1
};
props.cacheName=props.name+String(props.version);

{
    function fetchOnly(request){
        return fetch(request);
    }

    async function fetchAndCache(request){
        let cache=await caches.open(props.cacheName);
        let f=await fetchOnly(request);
        let rt=f.clone();
        await cache.put(request,f);
        return rt;
    }

    async function getCache(request){
        let cache=await caches.open(props.cacheName);
        return await cache.match(request);
    }

    function protoAllowed(requestURL){
        let url=new URL(requestURL);
        for(let proto of props.allowedProtocols){
            if(proto==url.protocol)return true;
        }
        return false;
    }

    function defaultHandler(request){
        if(!protoAllowed(request.url))return fetchOnly(request);
        let r=getCache(request);
        return r||fetchAndCache(request);
    }

    getResponse=(request)=>{
        if(request.cache){
            switch(request.cache){
                case "no-store":
                    return fetchOnly(request);
                case "reload":
                    return fetchAndCache(request);
                case "no-cache":
                    return fetchAndCache(request);
                case "only-if-cached": {
                    let r=getCache(request);
                    return r||new Response(undefined,{status:504});
                }
                default:
                    console.error(`Request has no cache mode ${request.cache}! ${request.url} `);
                    return defaultHandler(request);
            }
        }
    }
}

self.addEventListener("install", event=>{
    event.waitUntil(Promise.all([
        caches.open(props.cacheName)
    ]));
});

self.addEventListener("activate", event=>{
    event.waitUntil(Promise.all([
        clients.claim() //Reclamar el controlador durante la activacion
    ]));
});

self.addEventListener("fetch", event=>{
    let res=getResponse(event.request);
    event.respondWith(res);
});
