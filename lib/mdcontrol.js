"use-strict";
/* eslint no-underscore-dangle: "off" */
/* GLOBAL UTILITIES */
function _·(a){
    return document.querySelector(a);
}
let mdc=exports.mdc;
self.mdcontrol={};

/* Barra dinámica */
function dynamicBar(){
    self.mdcontrol.dynamicBar=new mdc.tabBar.MDCTabBar(_·("#barra_nav"));
    return self.mdcontrol.dynamicBar;
}

function afterLoad(a){
    return new Promise(resolver=>{
        exports.mdc.autoInit(a, resolver);
    });
}

class Dialogctl{
    constructor(a){
        this.defaults={
            url: "templates/dialog.vue",
            el: "#dialog-place",
            dialog_inner: "#multi_dialog",
            data: {
                dialog_title: "No title",
                dialog_body: "No content provided",
                dialog_accept: "Aceptar",
                dialog_cancel: "Cancelar"
            }
        };
        this.accept=["accept"];
        if(a)this.defaults=Object.assign({}, a);
        this.ready=this.init();
    }

    async init(){
        let loadObj=await load(this.defaults.url, ["noprefix", "type: text/plain"]);
        this.template=loadObj.text;
        this.app=new Vue({
            el: this.defaults.el,
            components: this.defaults.components,
            data: Object.assign({},this.defaults.data),
            methods: this.defaults.methods,
            template: this.template
        });
        this.component=new mdc.dialog.MDCDialog(_·(this.defaults.dialog_inner));
        return true;
    }

    async launch(a){
        await this.ready;
        for(let data in this.defaults.data){
            if(this.defaults.data.hasOwnProperty(data))this.app[data]=a[data]||this.defaults.data[data];
        }
        let x=new Promise(resolver=>{
            this.component.listen("MDCDialog:closed", event=>resolver(this.handler(event)));
        }).finally(()=>{
            this.component.unlisten("MDCDialog:closed");
        });
        this.component.open();
        return await x;
    }

    handler(event){
        let {action}=event.detail;
        return this.accept.includes(action);
    }

    async close(){
        await this.ready;
        this.component.close();
    }
}

if(exports) exports.mdcontrol={
    dynamicBar, //Barra dinámica para realizar ajustes
    dialog: new Dialogctl(), //Utilidad general para mostrar un diálogo
    afterLoad, //Hook para depués de cargar todos los elementos
    dialogoListas: new Dialogctl({ //Utilidad para el diálogo de listas
        el: "#dialogo-listas",
        dialog_inner: "#dialogo-listas__inner",
        url: "templates/dialogo_listas.vue",
        data: {
            nombre: "Lista sin nombre",
            lista: [{Día: ["Sin personas asignadas"]}],
            aceptar: "Guardar lista",
            cancelar: "Descartar lista",
            reverse: false
        }
    }),
    dialogoAsignables: new Dialogctl({ //Utilidad para el diálogo de personas asignables
        el: "#dialogo-asignables",
        dialog_inner: "#dialogo-asignables__inner",
        url: "templates/dialogo_asignables.vue",
        data: {
            data:{
                nombre: "Sin nombre",
                repite: false,
                veces_asignado: 0,
                dias: []
            },
            dias:[],
            aceptar: "Guardar",
            cancelar: "Descartar cambios",
            reverse: false
        }
    })
}
