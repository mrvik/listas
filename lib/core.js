/* global Dexie:readable */
/* eslint max-classes-per-file: ["error", 4] */
if(!self.Dexie)throw new Error("Dexie is not loaded");

var config; //eslint-disable-line init-declarations,vars-on-top
class Confctl{
    constructor(){
        let defaultConfig={
            db:[
                {
                    version: 0.1,
                    estructura: {
                        asignables: "++id,&nombre,*dias,repite,veces_asignado",
                        listas: "++id,&nombre,fecha"
                    }
                }
            ],
            dia_add: undefined,
            dias: this.generarDias()
        };
        let handler;
        handler={
            defineProperty: (...args)=>{
                this.updateLocalstorage();
                return Reflect.defineProperty(...args);
            },
            deleteProperty: (...args)=>{
                this.updateLocalstorage();
                return Reflect.deleteProperty(...args);
            },
            set: (obj, prop, value)=>{
                var val;
                if(typeof value=="object"){
                    val=new Proxy(value, handler);
                }
                this.updateLocalstorage();
                return Reflect.set(obj, prop, typeof val!="undefined"?val:value);
            }
        };
        this.handler=handler;
        this.updateLock=Promise.resolve();
        if(localStorage.hasOwnProperty("config")){
            this.config=JSON.parse(localStorage.config);
            this.legacyCheck();
        }else{
            this.config=defaultConfig;
        }
    }

    generarDias(){
        return ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"].map(nombre=>{
            return {
                nombre,
                personas: 0
            }
        });
    }

    legacyCheck(){
        if(typeof this.config.dias[0].nombre=="undefined"){
            let {asignados}=this.config;
            this.config.dias=this.config.dias.map(nombre=>{
                return {
                    nombre,
                    personas: asignados
                };
            });
            this.updateLocalstorage();
        }
    }

    async updateLocalstorage(){
        await this.updateLock;
        this.updateLock=this._updateLocalstorage();
    }

    async _updateLocalstorage(){
        localStorage.config=JSON.stringify(this.config);
    }
}
let confctl=new Confctl();
config=new Proxy(confctl.config, confctl.handler);
config.dias=new Proxy(confctl.config.dias.map(d=>new Proxy(d,confctl.handler)), confctl.handler);

function createdb(){
    return new Promise(resolver=>{
        self.db=new Dexie("listas");
        db.on("ready", ()=>resolver(self.db));
        config.db.forEach(a=>{
            self.db.version(a.version).stores(a.estructura);
        });
        db.open();
    });
}
async function crearLista(){
    return await db.transaction("rw", db.asignables, async ()=>{
        let salida={};
        for(let diaObj of config.dias){
            let dia=diaObj.nombre;
            let cantidad=diaObj.personas;
            salida[dia]=[];
            if(cantidad<1)continue;
            //Seleccionar a los asignados de la lista
            await db.asignables.orderBy("veces_asignado").and(a=>{
                if(a.dias.includes(dia))return true;
                return false;
            })
                .limit(cantidad)
                .modify(a=>{
                    if(salida[dia].includes(a.nombre))return;
                    salida[dia].push(a.nombre);
                    a.veces_asignado++;
                });
            if(salida[dia].length==cantidad)continue;
            console.warn("No ha habido suficientes personas. Repitiendo...");
            await db.asignables.orderBy("veces_asignado")
                .and(a=>{
                    if(a.repite&&a.dias.includes(dia))return true;
                    return false;
                })
                .limit(cantidad-salida[dia].length)
                .modify(a=>{
                    salida[dia].push(a.nombre);
                    a.veces_asignado++;
                });
            if(salida[dia].length!=cantidad)console.warn(`El día ${dia} ha quedado con ${cantidad-salida[dia].length} puestos por asignar`);
        }
        let listaTerminada={
            nombre: `Lista del ${(new Date()).toLocaleString()}`,
            lista: salida
        };
        console.log(salida);
        if(await Dexie.waitFor(exports.events.listas.confirmar(listaTerminada))){
            listaTerminada.nombre=exports.mdcontrol.dialogoListas.app.nombre;
            return listaTerminada;
        }
        throw new Error("Lista generada no aceptada por el usuario, transacción no confirmada");
    }).then(a=>{
        exports.events.db.asignables();
        return a;
    });
}
async function reloadData(){
    let data={
        listas: await exports.core.listsctl.listas,
        asignables: await exports.core.asignablesctl.asignables
    };
    Object.keys(data).forEach(a=>{
        exports.core.data[a]=data[a];
    });
    return data;
}
createdb().then(()=>{
    reloadData();
});

//Clases exportadas
class Listsctl{
    constructor(a){
        this.in=a;
    }

    get listas(){
        return this._getListas();
    }

    async _getListas(){
        let listas=await db.transaction("r!", db.listas, async ()=>{
            return await db.listas.toArray();
        });
        return listas;
    }

    async _setListas(...args){
        return await db.transaction("rw", db.listas, async ()=>{
            if(args[0]=="rm"){
                return await db.listas.delete(args[1].id);
            }
            return await db.listas.put(args[0], args[1]);
        }).then(a=>{
            exports.events.db.listas();
            return a;
        });
    }

    setListas(a, b){
        return this._setListas(a, b).catch(e=>{
            console.error("Ha habido un error al actualizar la base de datos de listas con los valores necesarios");
            console.error(e);
            throw e; //Let it go
        })
    }

    async clear(){
        return await db.transaction("rw", db.listas, async()=>{
            return await db.listas.clear();
        })
            .catch(e=>{
                console.error("No se ha podido limpiar la tabla de listas",e);
                throw e;
            })
    }

    async newList(){
        let lista=await crearLista();
        let obj={
            nombre: lista.nombre,
            fecha: Date.now(),
            lista: lista.lista
        };
        return await this.setListas(obj);
    }
}
class Asignablesctl{
    constructor(a){
        this.in=a;
    }

    get asignables(){
        return this._getAsignables();
    }

    async _getAsignables(){
        let asignables=await db.transaction("r", db.asignables, async ()=>{
            return await db.asignables.orderBy("nombre").toArray();
        });
        return asignables;
    }

    async _setAsignables(...args){
        let transaction=await db.transaction("rw", db.asignables, async ()=>{
            if(args[0]=="rm"){
                return await db.asignables.delete(args[1].id);
            }
            return await db.asignables.put(args[0], args[1]);
        });
        exports.events.db.asignables();
        return transaction;
    }

    async clearTimes(){
        let transaction=await db.transaction("rw", db.asignables, async()=>{
            return await db.asignables.toCollection().modify({veces_asignado:0});
        });
        exports.events.db.asignables();
        return transaction;
    }

    setAsignables(a, b){
        return this._setAsignables(a, b).catch(e=>{
            console.error("Ha habido un error al actualizar la base de datos de asignables con los valores requeridos");
            console.error(e);
            throw e; //Let it go
        });
    }
}

//Exportación de módulos
if(exports)exports.core={
    config,
    listsctl: new Listsctl(),
    asignablesctl: new Asignablesctl(),
    data: {
        reload: reloadData,
        listas: [],
        asignables: []
    }
};
