if(exports&&!exports.events){
    exports.events={};
    exports.events.db={};
}
function index__dbWatch(){
    if(exports&&exports.events&&exports.events.db){
        let dbWatch=["asignables", "listas"];
        dbWatch.forEach(a=>{
            exports.events.db[a]=async function(){
                await self.loading.index;
                self.exports.index.interfaz.updateData();
                return undefined;
            };
        });
    }
}
index__dbWatch();

async function afterRouteChange(route){
    if(route.path=="/")return app.$router.push(app.$router.options.routes[0].path)
    exports.mdcontrol.afterLoad();
    //Ruta por defecto -> Primera en declararse
    let actualTab=self.mdcontrol.dynamicBar.tabList_.filter(tab=>tab.root_.getAttribute("href")==`#${app.$route.fullPath}`)[0];
    if(actualTab){
        document.querySelectorAll(".mdc-tab-indicator--active").forEach(el=>el.classList.remove("mdc-tab-indicator--active"));
        actualTab.root_.querySelector(".mdc-tab-indicator").classList.add("mdc-tab-indicator--active");
        let actualIndex=self.mdcontrol.dynamicBar.tabList_.indexOf(actualTab);
        if(actualIndex>-1){
            self.mdcontrol.dynamicBar.activateTab(actualIndex);
        }
    }
}
if(exports&&exports.events)exports.events.afterRouteChange=afterRouteChange;

(async function(){
    class listas{
        constructor(){
            throw new Error("This class shall not be constructed");
        }

        static async confirmar(a){
            let dialogData={
                nombre: a.nombre||`Lista del ${(new Date()).toLocaleString()}`,
                lista: a.lista||a
            };
            return await exports.mdcontrol.dialogoListas.launch(dialogData);
        }
    }
    if(exports&&exports.events)exports.events.listas=listas;
})();

async function busquedaAsignables(target){
    let val;
    val=target.value;
    if(val&&val!="")return app.$router.push(`?search=${val}`);
    return app.$router.push("?");
}
if(exports&&exports.events)exports.events.busquedaAsignables=busquedaAsignables;
