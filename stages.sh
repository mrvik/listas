#!/bin/bash
pack(){
    git archive --format=tar -o $1 $2
}
unpack(){
    folder=build
    stat $folder&>/dev/null&&rm -rf $folder
    mkdir $folder
    tar -C $folder -xf $1
}
if [ "$1" == "pack-unpack" ]; then
    pack $2 $3||exit 1
    unpack $2||exit 2
    exit 0
fi

if [ "$1" == "generate" ];then
    if [[ "$2" == "" ]]; then
        echo "There's no dir to generate deploy to!">/dev/stderr
        exit 1
    fi
    stat $2&>/dev/null&&rm -rf dist; mkdir dist
    npm run build-scss
    xargs cp -r -v --target-directory=$2 <files.list
    node completeBuild.js $2
fi
