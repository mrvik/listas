"use-strict";
console.log("Cargado script de inicio");
self.main={};
function templates(){
    let plantillas=[
        "listas.vue",
        "exportar.vue",
        "opciones.vue",
        "asignables.vue"
    ];
    let tl=new loadlifyJS({
        properties:{
            prefix: "templates/"
        }
    });
    tl.flags.push("type: text/plain");
    return tl.load(plantillas);
}
async function createApp(a){
    console.log(a.routes);
    let data={
        opciones: ["Listas", "Exportar", "Asignables", "Opciones"],
        listas: await exports.core.listsctl.listas,
        asignables: await exports.core.asignablesctl.asignables,
        config: exports.core.config,
        exportar: [],
        dselected: null
    };
    let methods={
        newList: ()=>{
            return exports.core.listsctl.newList().catch(e=>{
                console.log(e);
            });
        },
        modLista: async (...args)=>{
            let confirmado=await exports.mdcontrol.dialogoListas.launch({
                nombre: args[0].nombre,
                lista: args[0].lista,
                aceptar: "Eliminar lista",
                cancelar: "Guardar cambios",
                reverse: true
            });
            if(confirmado)return await exports.core.listsctl.setListas("rm", args[0]);
            let lista=args[0];
            lista.nombre=exports.mdcontrol.dialogoListas.app.nombre;
            return await exports.core.listsctl.setListas(lista);
        },
        modAsignables: async (...args)=>{
            let opciones={
                data: args[0],
                dias:exports.core.config.dias,
                aceptar: "Eliminar persona",
                cancelar: "Guardar cambios",
                reverse: true
            };
            if(args[0]=="nuevo"){
                delete opciones.data;
                opciones.aceptar="Guardar persona";
                opciones.cancelar="Descartar persona";
                opciones.reverse=false;
            }
            let confirmado=await exports.mdcontrol.dialogoAsignables.launch(opciones);
            if(confirmado&&args[0]!="nuevo")return await exports.core.asignablesctl.setAsignables("rm", args[0]);
            if(!confirmado&&args[0]=="nuevo")return;
            let nuevo=Object.assign({},exports.mdcontrol.dialogoAsignables.app.data);
            return await exports.core.asignablesctl.setAsignables(nuevo).catch(e=>{
                let dialog_body=`Error desde la base de datos: ${e.message}`;
                if(e.message=="Unable to add key to index 'nombre': at least one key does not satisfy the uniqueness requirements."){
                    dialog_body="El campo 'nombre' debe de ser único en cada persona";
                }
                exports.mdcontrol.dialog.launch({
                    dialog_title: "Error al actualizar la base de datos",
                    dialog_body,
                    dialog_cancel: ""
                });
                console.error(e);
                exports.index.interfaz.updateData();
            });
        },
        addDia: (a)=>{
            console.log("Añadiendo día", a);
            let work=exports.core.config;
            let d={
                nombre: work.dia_add,
                personas: 0
            }
            work.dias.push(d);
            work.dia_add=undefined;
        },
        delDia: (...args)=>{
            args[0].preventDefault();
            let work=exports.core.config.dias;
            let index=work.indexOf(work.filter(n=>n.nombre==args[1])[0]);
            work.splice(index, 1);
        },
        limpiarListas: ()=>{
            let dialogData={
                dialog_title: "¿Borrar todas las listas?",
                dialog_body: "Esta acción no se puede deshacer",
                dialog_cancel: "Cancelar",
                dialog_accept: "Continuar"
            };
            exports.mdcontrol.dialog.launch(dialogData).then(res=>{
                if(res) return exports.core.listsctl.clear().then(exports.index.interfaz.updateData);
            });
        },
        clearTimes: ()=>{
            let dialogData={
                dialog_title: "¿Reiniciar veces asignados?",
                dialog_body: "Todos volverán a tener la misma prioridad. Esta acción no se puede deshacer",
                dialog_cancel: "Cancelar",
                dialog_accept: "Continuar"
            };
            exports.mdcontrol.dialog.launch(dialogData).then(res=>{
                if(res) return exports.core.asignablesctl.clearTimes();
            });
        },
        print: self.print,
        events: exports.events
    }
    for(let route of a.routes){
        route.props={data};
    }
    Vue.use(VueRouter);
    let router=new VueRouter({
        routes: a.routes,
        methods,
        data
    });
    console.log("Datos de la app: ", data);
    if(!data.listas)data.listas=[];
    let app=new Vue({
        router,
        data
    }).$mount("#app");
    await Vue.nextTick();
    return app;
}

//Parte de orquestación
(async function () {
    let mdcontrol=exports.mdcontrol;

    let routes=await templates().then(a=>{
        return a.map(dt=>{
            let name=dt.link.pathname.match(/(\/.*\/)(.*)(\.vue)/)[2];
            let rt={
                name,
                path: `/${name}`,
                component:{
                    template: dt.text,
                    props: ["data"]
                }
            };
            return rt;
        });
    });
    self.app=await createApp({
        routes
    });
    app.$router.afterEach(async ()=>{
        await Vue.nextTick();
        let route=app.$route;
        return await exports.events.afterRouteChange(route);
    });

    mdcontrol.dynamicBar();

    exports.events.afterRouteChange(app.$route)
    mdcontrol.afterLoad();
})();
class Interfaz{
    static updateData(){
        return exports.core.data.reload().then(data=>{
            for(let option in data){
                if(data.hasOwnProperty(option)){
                    self.app.$router.options.data[option]=data[option];
                }
            }
        });
    }
}

//Exportación de módulos
if(exports)exports.index={interfaz: Interfaz};
/* __Módulos exportados__
 * interfaz -> Comunicación desde otros scripts con métodos locales
 */
