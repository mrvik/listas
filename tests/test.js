#!/bin/env node
/* eslint-env node */
/* eslint-disable global-require, no-sync*/
var babel, fs, path;
try{
    path=require("path");
    fs=require("fs");
    babel=require("@babel/core");
}catch(e){
    console.error(e);
    throw new Error("Uno de los módulos requeridos no está disponible");
}

console.log("==> Iniciando test");
var errors=[]; //eslint-disable-line vars-on-top
let testFilter=/^.*\.js$/;
let testDir=[".", "lib/"];
testDir.forEach(a=>{
    let archivos=fs.readdirSync(a).filter(a=>a.match(testFilter));
    archivos.forEach(b=>{
        let archivo=path.resolve([a, b].join("/"));
        console.log(`Testeando ${archivo}`);
        try{
            babel.transform(fs.readFileSync(archivo, "UTF-8"));
            console.log(`${archivo} testeado correctamente`);
        }catch(e){
            console.error(`'${archivo}': Error at line ${e.loc.line} column ${e.loc.column}\n${e.message}`);
            errors.push(e);
        }
    });
});

if(errors.length)throw new Error(`There ${errors.length==1?"is":"are"} ${errors.length} error${errors.length!=1?"s":""}`);
console.log("Tests done, no errors");
